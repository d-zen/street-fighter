export class Fighter {
  id: string = null;
  name: string = null;
  health: number = null;
  attack: number = null;
  defense: number = null;
  source: string = null;
}
