import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class BattleLogService {
  logs: string[] = [];

  add(log: string) {
    this.logs.push(log);
  }

  clear() {
    this.logs = [];
  }
}
