import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FightersComponent } from './fighters/fighters.component';
import { FighterDetailComponent } from './fighter-detail/fighter-detail.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'fighter/:id', component: FighterDetailComponent },
  { path: 'fighters', component: FightersComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
