// modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { AppRoutingModule } from './app-routing.module';

// services
import { ModalService } from './modals/modal.service';
import { InMemoryDataService } from './in-memory-data.service';

// components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FighterDetailComponent } from './fighter-detail/fighter-detail.component';
import { FightersComponent } from './fighters/fighters.component';
import { FighterSearchComponent } from './fighter-search/fighter-search.component';
import { BattleLogsComponent } from './logs/battle-logs.component';

  // modal
  import { FighterModalComponent } from './modals/fighter-modal/fighter-modal.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    // HttpClientInMemoryWebApiModule.forRoot(
    //   InMemoryDataService, { dataEncapsulation: false }
    // )
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    FightersComponent,
    FighterDetailComponent,
    BattleLogsComponent,
    FighterSearchComponent,
    FighterModalComponent,
  ], providers: [
    ModalService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
