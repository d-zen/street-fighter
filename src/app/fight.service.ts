import { Injectable } from '@angular/core';
import {BattleLogService} from './battle-log.service';
import {Fighter} from './fighter';

@Injectable({
  providedIn: 'root'
})
export class FightService {

  constructor(
    private battleLogService: BattleLogService
  ) { }

  randomInt(min = 1, max = 2) {
    return min + Math.random() * (max - min);
    // return min + Math.random() * (max + 1 - min);
  }

  getHitPower(fighter: Fighter) {
    return fighter.attack * this.randomInt();
  }

  getBlockPower(fighter: Fighter) {
    return fighter.defense * this.randomInt();
  }

  fight(firstFighter, secondFighter) {
    let winner = null;

    while (!winner) {
      if (firstFighter.health > 0)  {
        secondFighter.health -= this.getHitPower(firstFighter) - this.getBlockPower(secondFighter);
        this.battleLogService.add(
          `${firstFighter.name} hit ${secondFighter.name}. Now ${secondFighter.name} has ${secondFighter.health} health!`);
      } else {
        winner = secondFighter.name;
        alert('Winner ' + winner + ' !');
      } if (secondFighter.health > 0) {
        firstFighter.health -= this.getHitPower(secondFighter) - this.getBlockPower(firstFighter);
        this.battleLogService.add(
          `${secondFighter.name} hit ${firstFighter.name}. Now ${firstFighter.name} has ${firstFighter.health} health!`);
      } else {
        winner = firstFighter.name;
        alert('Winner ' + winner + ' !');
      }
    }
  }

}
