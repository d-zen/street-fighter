import {Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import { AbstractModal } from '../modal.component';
import { ModalService } from '../modal.service';

@Component({
  selector: 'app-fighter-modal',
  templateUrl: './fighter-modal.component.html',
  styleUrls: [
    '../modal.component.scss',
    './fighter-modal.component.scss'
  ]
})

export class FighterModalComponent extends AbstractModal {
  public static readonly ID = 'fighter-modal';
  protected id = FighterModalComponent.ID;
  public fighter: any;

  constructor(
    protected modalService: ModalService,
    protected el: ElementRef
  ) {
    super(modalService, el);
  }

  open(fighter: any): void {
    this.fighter = fighter;
    this.isHidden = false;
    this.element.show();
  }

  close(): void {
    this.fighter = null;
    super.close();
  }

}

