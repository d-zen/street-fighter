import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { FighterSearchComponent } from '../fighter-search/fighter-search.component';

import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { FIGHTERS } from '../mock';
import { FighterService } from '../fighter.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let fighterService;
  let getHeroesSpy;

  beforeEach(async(() => {
    fighterService = jasmine.createSpyObj('FighterService', ['getFighters']);
    getHeroesSpy = fighterService.getFighters.and.returnValue( of(FIGHTERS) );
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        FighterSearchComponent
      ],
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        { provide: FighterService, useValue: fighterService }
      ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display "Top Heroes" as headline', () => {
    expect(fixture.nativeElement.querySelector('h3').textContent).toEqual('Top Heroes');
  });

  it('should call fighterService', async(() => {
    expect(getHeroesSpy.calls.any()).toBe(true);
    }));

  it('should display 4 links', async(() => {
    expect(fixture.nativeElement.querySelectorAll('a').length).toEqual(4);
  }));

});
