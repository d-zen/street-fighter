import {Injectable} from '@angular/core';
import {FighterService} from '../fighter.service';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class FightersRepository {
  public fighters: BehaviorSubject<any[]> = new BehaviorSubject([]);

  constructor(
    private fighterService: FighterService
  ) {
    this.getFighters();
  }

  getFighters() {
    this.fighterService.getFighters().subscribe(res => {
      this.fighters.next(res);
      return true;
    }, err => {
      console.log(err);
    });
  }
}
