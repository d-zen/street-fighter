import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Fighter } from './fighter';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const fighters = [
        {
            id: '1',
            name: 'Ryu',
            health: 45,
            attack: 4,
            defense: 3,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif'
        },
        {
            id: '2',
            name: 'Dhalsim',
            health: 60,
            attack: 3,
            defense: 1,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif'
        },
        {
            id: '3',
            name: 'Guile',
            health: 45,
            attack: 4,
            defense: 3,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/guile-hdstance.gif'
        },
        {
            id: '4',
            name: 'Zangief',
            health: 60,
            attack: 4,
            defense: 1,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/zangief-hdstance.gif'
        },
        {
            id: '5',
            name: 'Ken',
            health: 45,
            attack: 3,
            defense: 4,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/ken-hdstance.gif'
        },
        {
            id: '6',
            name: 'Bison',
            health: 45,
            attack: 5,
            defense: 4,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif'
        },
        {
            id: '7',
            name: 'Chun-Li',
            health: 40,
            attack: 3,
            defense: 8,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/chunli-hdstance.gif'
        },
        {
            id: '8',
            name: 'Blanka',
            health: 80,
            attack: 8,
            defense: 2,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/blanka-hdstance.gif'
        },
        {
            id: '9',
            name: 'E.Honda',
            health: 50,
            attack: 5,
            defense: 5,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/ehonda-hdstance.gif'
        },
        {
            id: '10',
            name: 'Balrog',
            health: 55,
            attack: 4,
            defense: 6,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/balrog-hdstance.gif'
        },
        {
            id: '11',
            name: 'Vega',
            health: 50,
            attack: 4,
            defense: 7,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/vega-hdstance.gif'
        },
        {
            id: '12',
            name: 'Sagat',
            health: 55,
            attack: 4,
            defense: 6,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/sagat-hdstance.gif'
        },
        {
            id: '13',
            name: 'Cammy',
            health: 45,
            attack: 4,
            defense: 7,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/cammy-hdstance.gif'
        },
        {
            id: '14',
            name: 'Fei Long',
            health: 80,
            attack: 2,
            defense: 3,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/feilong-hdstance.gif'
        },
        {
            id: '15',
            name: 'Dee Jay',
            health: 50,
            attack: 5,
            defense: 5,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/deejay-hdstance.gif'
        },
        {
            id: '16',
            name: 'T.Hawk',
            health: 60,
            attack: 5,
            defense: 3,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/thawk-hdstance.gif'
        },
        {
            id: '17',
            name: 'Akuma',
            health: 70,
            attack: 5,
            defense: 5,
            source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/akuma-hdstance.gif'
        }
    ];
    return {fighters: fighters};
  }

  // Overrides the genId method to ensure that a fighter always has an id.
  // If the fighters array is empty,
  // the method below returns the initial number (11).
  // if the fighters array is not empty, the method below returns the highest
  // fighter id + 1.

  // genId(fighters: Fighter[]): number {
  //   return fighters.length > 0 ? Math.max(...fighters.map(fighter => fighter.id.toString())) + 1 : 11;
  // }
}
