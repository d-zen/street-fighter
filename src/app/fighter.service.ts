import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {Fighter} from './fighter';
import {BattleLogService} from './battle-log.service';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({providedIn: 'root'})
export class FighterService {

  private url = 'http://localhost:3000';  // URL to web api
  private _fighters: BehaviorSubject<any[]> = new BehaviorSubject([]);


  constructor(
    private http: HttpClient,
    private battleLogService: BattleLogService
  ) {
    this.getFighters().subscribe(res => {
      this._fighters.next(res);
    });
  }

  get fighters(): any[] {
    return this._fighters.getValue();
  }

  get fightersSubject(): BehaviorSubject<any[]> {
    return this._fighters;
  }

  /** GET fighters from the server */
  getFighters(): Observable<Fighter[]> {
    const url = `${this.url}/fighter`;
    return this.http.get<Fighter[]>(url)
      .pipe(
        tap(_ => this.log('fetched fighters')),
        catchError(this.handleError<Fighter[]>('getFighters', []))
      );
  }

  /** GET fighter by id. Return `undefined` when id not found */
  getFighterNo404<Data>(id: number): Observable<Fighter> {
    const url = `${this.url}/?id=${id}`;
    return this.http.get<Fighter[]>(url)
      .pipe(
        map(fighters => fighters[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} fighter id=${id}`);
        }),
        catchError(this.handleError<Fighter>(`getFighter id=${id}`))
      );
  }

  /** GET fighter by id. Will 404 if id not found */
  getFighter(id: number): Observable<Fighter> {
    const url = `${this.url}/${id}`;
    return this.http.get<Fighter>(url).pipe(
      tap(_ => this.log(`fetched fighter id=${id}`)),
      catchError(this.handleError<Fighter>(`getFighter id=${id}`))
    );
  }

  /* GET fighters whose name contains search term */
  searchFighters(term: string): Observable<Fighter[]> {
    if (!term.trim()) {
      // if not search term, return empty fighter array.
      return of([]);
    }
    return this.http.get<Fighter[]>(`${this.url}/?name=${term}`).pipe(
      tap(_ => this.log(`found fighters matching "${term}"`)),
      catchError(this.handleError<Fighter[]>('searchFighters', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new fighter to the server */
  addFighter(fighter: Fighter): Observable<Fighter> {
    return this.http.post<Fighter>(this.url, fighter, httpOptions).pipe(
      tap((newFighter: Fighter) => this.log(`added fighter w/ id=${newFighter.id}`)),
      catchError(this.handleError<Fighter>('addFighter'))
    );
  }

  /** DELETE: delete the fighter from the server */
  deleteFighter(fighter: Fighter | number): Observable<Fighter> {
    const id = typeof fighter === 'number' ? fighter : fighter.id;
    const url = `${this.url}/${id}`;

    return this.http.delete<Fighter>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted fighter id=${id}`)),
      catchError(this.handleError<Fighter>('deleteFighter'))
    );
  }

  /** PUT: update the fighter on the server */
  updateFighter(fighter: Fighter): Observable<any> {
    return this.http.put(this.url, fighter, httpOptions).pipe(
      tap(_ => this.log(`updated fighter id=${fighter.id}`)),
      catchError(this.handleError<any>('updateFighter'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a FighterService message with the BattleLogService */
  private log(log: string) {
    this.battleLogService.add(`FighterService: ${log}`);
  }
}
