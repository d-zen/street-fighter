import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FightersComponent } from './fighters.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FightersComponent', () => {
  let component: FightersComponent;
  let fixture: ComponentFixture<FightersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FightersComponent ],
      imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FightersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
