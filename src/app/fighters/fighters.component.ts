import {Component, OnInit} from '@angular/core';

import {FighterService} from '../fighter.service';
import {Fighter} from '../fighter';
import {FightersRepository} from '../repository/fighters.repository';
import {ModalService} from '../modals/modal.service';
import {FighterModalComponent} from '../modals/fighter-modal/fighter-modal.component';
import {FightService} from '../fight.service';

@Component({
  selector: 'app-fighters',
  templateUrl: './fighters.component.html',
  styleUrls: ['./fighters.component.scss']
})
export class FightersComponent implements OnInit {
  fighters: Fighter[];

  public selectedFirstFighter: Fighter;
  public selectedSecondFighter: Fighter;

  constructor(
    private fighterService: FighterService,
    private fightersRepository: FightersRepository,
    private fightService: FightService,
    private modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.setFighters();
  }

  startFight() {
    this.fightService.fight(this.selectedFirstFighter, this.selectedSecondFighter);
  }

  setFighters() {
    this.fighterService.fightersSubject.subscribe(fighters => {
      this.fighters = fighters;
      if (this.fighters.length) {
        this.selectedFirstFighter = this.fighters[0];
        this.selectedSecondFighter = this.fighters[0];
      }
    });
  }

  selectFirstFighter(fighter: any) {
    this.selectedFirstFighter = fighter;
  }

  selectSecondFighter(fighter: any) {
    this.selectedSecondFighter = fighter;
  }

  getFighters(): void {
    this.fighterService.getFighters()
      .subscribe(fighters => {
        this.fighters = fighters;
        if (!this.selectedSecondFighter || !this.selectedFirstFighter) {
          this.selectedFirstFighter = this.fighters[0];
          this.selectedSecondFighter = this.fighters[0];
        }
      }, err => {
        console.log(err);
      });
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.fighterService.addFighter({name} as Fighter)
      .subscribe(fighter => {
        this.fighters.push(fighter);
      });
  }

  delete(fighter: Fighter): void {
    this.fighters = this.fighters.filter(h => h !== fighter);
    this.fighterService.deleteFighter(fighter).subscribe();
  }

  editFirstFighter() {
    this.modalService.open(FighterModalComponent.ID, this.selectedFirstFighter);
  }
  editSecondFighter() {
    this.modalService.open(FighterModalComponent.ID, this.selectedSecondFighter);
  }

}
